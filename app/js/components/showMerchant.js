'use strict';


import TemplateG from "./templateGenerator";
const tem = new TemplateG();

/**
* @class {ShowMerchant} - constructor
* renders the data from the server in a table view 
*/
class ShowMerchant 
{
	/**
	* Render the view with received data
	* Prices will display in EUR and will be orderd by amount
	*/
	prepareData(data) {
		if (!data.length >= 1) return "Error: No data received";		
		const merchantsToDisplay = [];
		//loop through the records
		data.forEach((val) => {	
			if (val.merchant)
				return (val.merchant.match(/Ginger/gi)) ? merchantsToDisplay.push(val) : "";
			else 
				return "";
		});
		return merchantsToDisplay;
	}

	/**
	* Render the HTML in the view
	* @param {data} - object with the payments
	* @param {context} - the context where the data will be displayed
	*/
	render(data, context) {
		if (!context.length >=1 ) return "Error: the context could not be found";
		context = context[0];
		context.innerHTML = "";
		
		data = this.prepareData(data);
		//render the data template
		tem.renderTable(data, context);
	}
}

export default ShowMerchant;