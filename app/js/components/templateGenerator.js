'use strict';

/**
* @class {TemplateGenerator} - constructor
* renders the data from the server in a table view 
*/
class TemplateGenerator 
{
	/**
	* Render the HTML in the view
	* @param {data} - object with the payments
	* @param {context} - the context where the data will be displayed
	*/
	renderTable(data, context) {
		context.innerHTML = "";
        //create the holder element for the table
        if (!context.children.length >=1 ) {
        	//create elements
        	const parentHolder = document.createElement('div');
        	parentHolder.className = "payments_holder";
        	context.appendChild(parentHolder);	
        	//prepare row title
        	let rowTitle = document.createElement("div");
			rowTitle.className = "title row";
			parentHolder.appendChild(rowTitle);

			//loop throug data and display it
			for (let item in data) 
			{
				let date = new Date(data[item].created),
					day = date.getDate(),
					monthIndex = date.getMonth(),
					year = date.getFullYear(),	
					hour = date.getHours(),	
					min = date.getMinutes(),	
					titleDiv = document.createElement("div"); //Header row of the table
				//table titles
				rowTitle.appendChild(titleDiv);
				rowTitle.innerHTML = "<div>Method</div>" +
					"<div>Amount</div>" +
					"<div>Currency</div>" +
					"<div>Created</div>" +
					"<div>Merchant</div>" +
					"<div>Status</div>";

				//createt the row content
				let rowContent = document.createElement("div");
				rowContent.className = "content row";
				//create the children of the content
				let contentDiv = document.createElement("div");
				rowContent.appendChild(contentDiv);				
				parentHolder.appendChild(rowContent);
				rowContent.innerHTML = "<div>" + data[item].method + "</div>" +
					"<div>" + data[item].amount / 100 + "</div>" +
					"<div>" + data[item].currency + "</div>" +
					"<div>" + day + "/" + monthIndex + "/" + year + "<br/>@"+hour+":" + min + "</div>" +
					"<div>" + data[item].merchant + "</div>" +
					"<div>" + data[item].status + "</div>";
			}
		}
	}
}

export default TemplateGenerator;