'use strict';

import filterPayments 	from './filterPayments';
import AppActions 		from '../actions/app.actions';


const filter = new filterPayments();
const appActions = new AppActions();

/**
* @class {AddPayments} - creates POST request
* receives the element where the action will happen
* uses a static data to be sent to the server
* render the html form
* create the form action
*/
class AddPayments {

	constructor() {
		this.payMethods = [];
		this.merchants = [];
		this.currency = [];
	}

	/**
	* Set's data needed for this form
	* @param {object} - data from server to be filtered
	*/
	getPayMethods(data) {
		if (!data.length >= 1) return "Error: No data received";
		data.filter((item) => {
			this.payMethods.push(item.method);
			this.merchants.push(item.merchant);
			this.currency.push(item.currency);
		});
	}

	renderForm() {
		//Form holder element
		const contentHolder = document.createElement("div");
		//form element
		const postForm = document.createElement("form");
		postForm.action = "#";
		postForm.className = "submit_form";

		//input amount
		const inputAmount = document.createElement("input");
		inputAmount.type = "text";
		inputAmount.className = "amount";
		inputAmount.placeholder = "Ammount:";

		//submit button
		const inputButton = document.createElement("button");
		inputButton.innerHTML = "Submit";

		//get the select box for payment methods
		let payMethodsSelect = filter.renderSelectBox(this.payMethods, "payment method");
		//get the select box for payment methods
		let merchantsSelect = filter.renderSelectBox(this.merchants, "merchant");
		//render select box with currency
		let currencySelect = filter.renderSelectBox(this.currency, "currency");

		postForm.appendChild(inputAmount);
		//append the methods select
		postForm.appendChild(payMethodsSelect);
		//append the methods select
		postForm.appendChild(merchantsSelect);
		//append the currency select
		postForm.appendChild(currencySelect);
		//append the button
		postForm.appendChild(inputButton);
		//append the form
		contentHolder.appendChild(postForm);

		inputButton.addEventListener('click', (e) => {
			this.captureSubmitForm(postForm);
		})

		return contentHolder;
	}

	/**
	* Capture the submit event 
	* validate the form
	* get the values and send them to addPayment action
	*/
	captureSubmitForm(form) 
	{	
		let errorDiv = document.createElement("div");
		errorDiv.display = "none";
		errorDiv.innerHTML = "Please fill in the fileds";
		form.appendChild(errorDiv);

		//validate payment method
		let validateMethod = form.querySelectorAll(".paymentmethod")[0].value;
		if (validateMethod.match(/select an/gi)) {
			console.log("select a payment method");	
			errorDiv.display = "block";
			//show error msg
			return false;
		} 

		//validate payment method
		let validateMerchant = form.querySelectorAll(".merchant")[0].value;
		if (validateMerchant.match(/select an/gi)) {
			console.log("select a merchant");	
			errorDiv.display = "block";
			//show error msg
			return false;
		} 

		let validateCurrency = form.querySelectorAll(".currency")[0].value;
		if (validateCurrency.match(/select an/gi)) {
			console.log("select currency");	
			errorDiv.display = "block";
			//show error msg
			return false;
		} 

		let validateAmmount = form.querySelectorAll(".amount")[0].value;
		if (validateAmmount === "" || isNaN(validateAmmount)) {
			console.log("Add amount as int pls");
			errorDiv.display = "block";
			//show error msg
			return false;
		} 
		//data to be sent to server
		let dataObj =  {
			"created": new Date(),
	      	"method": validateMethod,
	      	"amount": validateAmmount,
	      	"currency": validateCurrency,
	      	"merchant": validateMerchant
	    }
	    //add action to save to db
		let req = appActions.actionAdd(dataObj);
		req.then((request) => {
			//there is data saved so hide the form
			if (request.length >= 1) {
				form.parentNode.innerHTML = "Thank you for submitting";
			}
		})
	}

	render(data, el) 
	{
		//reset the content when accessing the view
		el.innerHTML = "";
		//set data that is needed for this form
		this.getPayMethods(data);
		el.appendChild(this.renderForm());
	}

}

export default AddPayments;