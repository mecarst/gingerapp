'use strict';

import TemplateG from "./templateGenerator";
const tem = new TemplateG();

/**
* @class {GetPayments} - constructor
* renders the data from the server in a table view 
* has default a limit of 20
*/
class GetPayments 
{
	constructor() {
		this.currency = "EUR";
		this.rates = {
			"EUR" : 1, // 1 EUR === 1.14  USD 
			"GBP" : 1.277,
			"USD" : 0.8798,
			"AUD" : 0.6478
		}
		this.data = [];
		//max number of paymnets
		this.rowLimit = 20;
	}

	/**
	* Render the view with received data
	* Prices will display in EUR and will be orderd by amount
	*/
	prepareData(data) {
		if (!data.length >= 1) return "Error: No data received";
		const paymentsToDisplay = [];
		//limit the data
		data = data.slice(0, this.rowLimit);
		//return the highest price first
		data.sort(this.sortAmount).reverse();
		//loop through the records
		data.forEach((val) => {
			val.amount = val.amount / 100;
			//OPTIONAL TO CONVERT USD -> EUR etc.
			// this.convertCurrency(val.currency, amount);
			paymentsToDisplay.push(val);
			this.data.push(val);
		});
		return paymentsToDisplay;
	}

	/**
	* Convert the prices in EUR (makes more sens on display the prices to be in EUR)
	* @param {currency} - string currency type
	* @param {amount} - INT 
	*/
	convertCurrency(currency, amount) {
		//convert currency to EUR
		switch (currency) {
			case "EUR":
				amount = amount + 1;
				break;
			case "USD":
				amount = (amount * this.rates.USD).toFixed(2);
				break;
			case "GBP":
				amount = (amount * this.rates.GBP).toFixed(2);
				break;
			case "AUD": 
				amount = (amount * this.rates.AUD).toFixed(2);
				break;
			default:
				amount = 1;
				return;
		}
		return amount;
	}

	/**
	* Order higher > lower
	* @return (int) - calculated value
	*/
	sortAmount(a, b) {
		return a.amount - b.amount;
	}

	/**
	* Render the HTML in the view
	* @param {data} - object with the payments
	* @param {context} - the context where the data will be displayed
	*/
	render(data, context) {
		if (!context.length >=1 ) return "Error: the context could not be found";
		context = context[0];
		context.innerHTML = "";
		if (this.data.length >= this.rowLimit) {
			data = this.prepareData(data);
		}
		tem.renderTable(data, context);
	}
}

export default GetPayments;