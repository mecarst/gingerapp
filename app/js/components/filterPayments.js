'use strict';

import TemplateG from "./templateGenerator";
const tem = new TemplateG();

/**
* @class {FilterPayments} - constructor
* renders select option
* render the list of data
*/
class FilterPayments 
{
	constructor() {
		this.payMethods = [];
		this.data = [];
		this.context = null;
		this.selectBox = null;
		this.filterHolder = null;
	}
	/**
	* Render the view with received data
	* Prices will display in EUR and will be orderd by amount
	*/
	prepareData(data) {
		if (!data.length >= 1) return "Error: No data received";
		const merchantsToDisplay = [];
		data.filter((item) => {
			this.payMethods.push(item.method);
			merchantsToDisplay.push(item);
		})
		return merchantsToDisplay;
	}

	/**
	* Render the select box HTML in the view
	* appends methods of payment in the options
	* add's event listener for mapping the data based on selector
	*/
	renderSelectBox(methods, selectString) {
		let x = 0, selectEl = this.selectBox = document.createElement("select"),
			dataContext = document.createElement("div"),
			defaultOpt = "<option>Select a " + selectString + "</option>";
		//set class name on the select box
		selectEl.className = selectString.replace(" ", "");
		//insert the first option as default
		selectEl.innerHTML = defaultOpt;
		//insert the select
		dataContext.appendChild(selectEl);
		this.filterHolder = dataContext;
		//loop through array and remove duplicates
		methods.filter((item, pos) => {
			//make sure thare are no undefined's
			//the match is because of bad entries in db ;)
			if (typeof item !== "undefined" && !item.match(/select an/ig)) 
			{
				if (methods.indexOf(item) == pos) {
					let option = "<option value=" + item + " class='method'>" + item + "</option>";
					selectEl.innerHTML += option;
				}
			}
		});
		return dataContext;
	}

	/**
	* Render the HTML in the view
	* @param {data} - object with the payments
	* @param {context} - the context where the data will be displayed
	*/
	render(data, context) {
		if (!context.length >=1 ) return "Error: the context could not be found";
		//main section holder
		context = this.context = context[0];
		context.innerHTML = "";
		//data from the server
		data = this.data = this.prepareData(data);
		//create the select options for merchants
		this.renderSelectBox(this.payMethods, "payment method");

		//prepare the data and elements
		let selectBox = this.selectBox, selectedData = [], self = this,
			dataHolder = document.createElement("div");
		//clean the content 
		dataHolder.innerHTML = "";
		//append the dataHolder div
		this.filterHolder.appendChild(dataHolder);		
		//append the whole filterholder
		this.context.appendChild(this.filterHolder);
		//trigger on change method
		selectBox.addEventListener("change", (e) => {
			if (selectedData.length >=1 ) selectedData = [];
			self.data.filter( (item, pos) => {
				if (item.method == selectBox.value) {
					selectedData.push(item);
				}
			});
			dataHolder.innerHTML = "";
			tem.renderTable(selectedData, dataHolder);
		});
	}

}

export default FilterPayments;