'use strict';


import QueryData 		from './api/query';
import AppActions 		from './actions/app.actions';
import appConstants 	from './constants/app.constants';

import AddPayments 		from "./components/addPayments";
import GetPayments 		from "./components/getPayments";
import ShowMerchant 	from "./components/showMerchant";
import FilterPayments 	from "./components/filterPayments";


const appActions = new AppActions();
const getPayments = new GetPayments();
const showMerchant = new ShowMerchant();
const filterPayments = new FilterPayments();
const addPayments = new AddPayments();


/**
* @class {GingerAssignment} - App entry point 
*/
export default class GingerAssignment {
	constructor() {				
		this.getButtons();
		this.layout = this.getPageSection();
		this.serverData = [];

		//GET data from the server and populate the callback view
		if (this.serverData.length <= 0) {
			appActions.actionGet("GET").then((data) => {
				this.serverData.push(data);
			});
		}
	}

	/**
	* Grab the elements in the page 
	* each will bind to there own action
	*/
	getButtons() {
		let buttons = document.querySelectorAll('aside > button'),
			x = 0, len = buttons.length;
		for (x, len; x < len ; x++) {
			buttons[x].addEventListener('click', this.triggerButtonAction.bind(this), false);
		}
	}

	getPageSection() {
		let sections = document.querySelectorAll('section');
		return sections;
	}

	/** 
	* @param {e} - mouse event
	* Triggers the methods of the button to perform the required action
	*/
	triggerButtonAction(e) {
		e.preventDefault();
		let methodAction = e.target.getAttribute("data-action");
		switch (methodAction) {
			case appConstants.PAYMENTS:
				this.showPaymentsList();
				break;
			case appConstants.PROMISES:
				this.showMerchantList();
				break;
			case appConstants.FILTER: 			
				this.filterPaymentMethod();
				break;
			case appConstants.ADD:
				this.addPayments();
				break;
			default:
				return false;
		}
	}

	/**
	* Calls the render method of Payments View Table
	*/
	showPaymentsList() {
		getPayments.render(this.serverData[0], this.layout);
	}

	/**
	* Calls the render method of Merchants View Table
	*/
	showMerchantList() {
 		showMerchant.render(this.serverData[0], this.layout);
	}

	/**
	* Calls the render method of Filter View Table	
	*/
	filterPaymentMethod() {
		filterPayments.render(this.serverData[0], this.layout);
	}
	/**
	* Create the POST form
	* Send the data to the server
	*/
	addPayments() {
		addPayments.render(this.serverData[0], this.layout[0]);
	}
}