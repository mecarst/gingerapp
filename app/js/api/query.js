'use strict';

/**
* Class for setting, getting data from uri's
*/
class QueryData {

	/**
	* Get data
	* @param {method} - method type (GET ,POST etc.)
	* @param {uri} - url to be accessed
	* @return {promise}
	*/
	getData(method, uri) {
		return this.makeCall(method, uri);
	}

	/**
	* Set data
	* @param {method} - method type (GET ,POST etc.)
	* @param {uri} - url to be accessed
	* @param {data} - data to be sent to server
	* @return {promise}
	*/
	setData(method, uri, data) {
		return this.makeCall(method, uri, data);
	}

	/**
	* Prepare the XML HTTP object that will be later used for ajax calls
	* @return {xmlHttp} - object
	*/
	prepareXmlHTTP() {
		var xmlHttp;
        try {
            // Firefox, Opera 8.0+, Safari
            xmlHttp = new XMLHttpRequest();
        } catch (e) {
            // Internet Explorer
            try {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {
                    alert("Your browser does not support AJAX!");
                    return false;
                }
            }
        }
        return xmlHttp;
	}

	/**
	* Create the call with promises
	* @param {method} - method type (GET ,POST etc.)
	* @param {uri} - url to be accessed
	*/
	makeCall(method, uri, data) 
	{    
		//create a promise 
		return new Promise((resolve, reject) => {
			var req = this.prepareXmlHTTP();
			method = method.toLowerCase();
			req.open(method, uri);
			req.onload = () => {
				//check for statuses
				(req.status == 200 || req.status == 201) ? resolve(req.response) : reject(Error(req.statusText));
			}
			// Handle network errors
	    	req.onerror = () => {
	      		reject(Error("Network Error"));
	    	};

	    	if (method === "post") {
	    		req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				req.send(data);
	    	} else {
		    	req.send();
	    	}
	    });
    }

}

export default QueryData;