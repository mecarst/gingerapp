'use strict';

import QueryData from '../api/query';
import AppConstants from '../constants/app.constants';

//init the query class
const promise = new QueryData();

/**
* @class {AppActions} - holds the data request actions
* User Interaction actions
*/
class AppActions 
{
	constructor() {
		this.uri = AppConstants.REQUEST_URI;
	}

	/**
	* When this button is clicked get the 20 payments with the highest amount 
	* @return {object} - data object from server
	*/
	actionGet(method) {		
		//get the payments
		return promise.getData(method, this.uri.PAYMENTS).then(JSON.parse).then( (res) => {		
			return (res.length >= 1) ? res : "";
		}, (error) => {
			//stop it all
			throw "Failed!" + error;
		});
	}

	/**
	* Does a POST request with data that will be saved
	* @param {object} - data that has been sent from the add payment
	*/
	actionAdd(data) {
		//prepare the uri data for POST
		data = "amount=" + data.amount + 
				"&currency=" + data.currency + 
				"&created=" + data.created + 
				"&merchant=" +data.merchant +
				"&method=" +data.method;
		return promise.setData("POST", this.uri.PAYMENTS, data).then( (res) => {
			return (res.length >= 1) ? res : "";
		}, (error) => {
			throw "Failde to insert " + error;
		});
	}
}

export default AppActions;