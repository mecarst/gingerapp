'use strict';

import GingerAssignment from './ginger';
import FastClick from "fastclick";

//Executes on loaded content or DOM complete events
function run() {
    const gingerApp = new GingerAssignment();
}

const LoadedStates =['complete', 'loaded', 'interactive'];

//Run the application when both DOM is ready and page content is loaded
Promise.all([
    new Promise((resolve, reject) => {
        //non IE browsers
        if (window.addEventListener) {
            window.addEventListener('DOMContentLoaded', resolve);
        } else {
            window.attachEvent('onload', resolve);
        }
    }).then( (val) => {
        // Make taps on links and buttons work fast on mobiles
        FastClick.attach(document.body);
    })
//when the DOM is ready call the run fn    
]).then( (ev) => {
    if (LoadedStates.includes(document.readyState) && document.body) run()
    else window.addEventListener('DOMContentLoaded', run, false);
});