'use strict';


const AppConstants = {	
	//Buttons ACTIONS
	PAYMENTS	: "callback_payments",
	PROMISES	: "promise_get",
	FILTER		: "filter_payment",
	ADD			: "add_payment",
	REQUEST_URI : {
		URI		: "http://localhost:3000/",
		DB		: "http://localhost:3000/db",
		PAYMENTS: "http://localhost:3000/payments"
	}
};

export default AppConstants;

