'use strict';

import webpack 				from 'webpack';
import WebpackDevServer 	from "webpack-dev-server";
import webpackHotMiddleware from 'webpack-hot-middleware';
import path 				from 'path';

//dirs
const BUILD_DIR = path.resolve(__dirname, 'build');
const APP_DIR = path.resolve(__dirname, 'app/js');

//outputs
const entryFile 	= "index.js";
const outputFile 	= "bundle.js";

const webpackConfig = {
	context: path.resolve(__dirname, './'),
	cache: true,
	debug: false,
	stats: {
		colors: true
	},
	devtool: false,
	plugins: [
	    new webpack.optimize.OccurenceOrderPlugin(),
    	new webpack.NoErrorsPlugin(),
	],
	resolve: {
		modulesDirectories: ["web_loaders", "web_modules", "node_loaders", "node_modules", "./node_modules"],
		extensions: ['', '.webpack.js', '.web.js', '.js']
	},
	entry: [
		'babel-polyfill',
		'webpack/hot/dev-server',
        'webpack-hot-middleware/client?reload=true',
		APP_DIR + "/" + entryFile
	],
	output: {
        path:  BUILD_DIR,
        publicPath: BUILD_DIR,
        filename: 'bundle.js'
    },
	module: {
		loaders: [
			{
				loader: "babel-loader",
				test: /(\.jsx|\.js)$/,
				exclude: /node_modules/,				
				query: {
					presets: ['es2015', "stage-0", 'es2016']
				},
				include: path.join(__dirname, '.')
			}			
		]
	}	
}

/**
 * Configuration for the client-side bundle
 */
const clientConfig = Object.assign({}, webpackConfig, {
	plugins: [
		new webpack.DefinePlugin({
			"process.env": {
				"NODE_ENV": JSON.stringify("development")
			}
		}),
    	new webpack.HotModuleReplacementPlugin(),
    	new webpack.NoErrorsPlugin()
	],
	// modify some webpack config options
	devtool : "eval",
	cache: false,
	watch: true,
	debug: true,
	console: true,
	stats: {
        assets: true,
        colors: true,
        version: false,
        hash: true,
        timings: false,
        chunks: false,
        chunkModules: false
    }
});

export {webpackConfig, clientConfig};
