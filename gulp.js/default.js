'use strict';

/*
* Default gulp tasks
*/
import gulp 		from 'gulp';
import gulpSequence from 'gulp-sequence';

//dafault tasks
const defaultTasks = ["styles", "build:dev"];

gulp.task('default', (cb) => {
	console.log("Running default Tasks");
	gulpSequence(defaultTasks, cb)
});