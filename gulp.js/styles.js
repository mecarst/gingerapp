'use strict';

import gulp 		from 'gulp';
import gulpif 		from 'gulp-if';
import browserSync 	from 'browser-sync';
import sass 		from 'gulp-sass';
import sourcemaps   from 'gulp-sourcemaps';
import autoprefixer from 'gulp-autoprefixer';
import nano 		from 'gulp-cssnano';
import path 		from 'path';
import notify 		from 'gulp-notify';
import plumber 		from 'gulp-plumber';
import concat 		from 'gulp-concat'

let reload = browserSync.reload;

gulp.task("styles", () => {
	browserSync.notify('Compiling SASS');
	return gulp.src("app/sass/**/*")
		.pipe(plumber())
		.pipe(sass({
			sourceComments: false,
			outputStyle: 'nested',
			includePaths: "app/sass/**/*",
			bundleExec:true
		}))
		.pipe(plumber.stop())
		.pipe(gulp.dest("build/"));
		// .pipe(notify({message : "SCSS completed"}));
});