'use strict';

import path 			from 'path';
import gulp 			from 'gulp';
import gutil 			from 'gulp-util';
import gulpSequence 	from 'gulp-sequence';
import browserSync 		from 'browser-sync';
import jsonServer 		from 'json-server';

browserSync.create();

//Webpack
import webpack 				from 'webpack';
import WebpackDevServer 	from 'webpack-dev-server';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

import {webpackConfig, clientConfig}	from '../webpack.config.babel';

const BUILD_DIR = path.resolve(__dirname, 'build/');
const APP_DIR = path.resolve(__dirname, 'app/src/js/');
const compiler = webpack(clientConfig);

/**
* Development build
*/
gulp.task("build:dev", ["webpack:build-dev", "styles"]);
gulp.task("webpack:build-dev", (callback) => {
	// run webpack
	compiler.run( (err, stats) => {
		if(err) throw new gutil.PluginError("webpack:build-dev", err);
		gutil.log("[webpack:build-dev]", stats.toString({
			colors: true
		}));
		callback();
	});
});
//serve task with watch
//opens browsersync withc will use the json-server proxy
gulp.task("serve", ["webpack-dev-server"]);
gulp.task("webpack-dev-server", ["build:dev"], (cb) => {
	let bundleStart;
	const reload = browserSync.reload;

	compiler.plugin('compile', function() {
		console.log('Compiling...');
		bundleStart = Date.now();
   	});

	compiler.plugin('done', function (stats) {
	    if (stats.hasErrors() || stats.hasWarnings()) {
	        return browserSync.sockets.emit('fullscreen:message', {
	            title: "Webpack Error:",
	            body:  stripAnsi(stats.toString()),
	            timeout: 100000
	        });
	    }
	   	console.log('Bundled in ' + (Date.now() - bundleStart) + 'ms!');
	    browserSync.reload();
	});
	
	/**
	 * Run Browsersync and use middleware for Hot Module Replacement
	 */
	browserSync.init({
		server: {
			baseDir: './'		
		},
		port: 8080,
		browser: ["google chrome"],
		// including full page reloads if HMR won't work
		files: [
			'app/js/**/*',
			'index.html'
		]
	}, (err, bs) => {
		// var jsonServer = require('json-server')
		var server = jsonServer.create()
		var router = jsonServer.router('db.json')
		var middlewares = jsonServer.defaults()

		server.use(middlewares)
		server.use(router)
		server.listen(3000, function () {
		  console.log('JSON Server is running')
		})

    	// access the browserSync connect instance
	    // bs.app.use(jsonServer.defaults());
    	// bs.app.use('/payments', jsonServer.router('db.json'));
	});

//JS
	gulp.watch("app/js/**/*", ['build:dev']).on('change', reload);
//CSS
	gulp.watch("app/**/*", ['styles']).on('change', reload);

});
