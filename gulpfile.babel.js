'use strict';

import fs 			from 'fs';
import path 		from 'path';

function scriptFilter (name) {
  	return /(\.(js)$)/i.test(path.extname(name));
};

//run project specifyc tasks
const tasks = fs.readdirSync('gulp.js/').filter(scriptFilter);
tasks.forEach((task) => { 
	require("./gulp.js/" + task)
});